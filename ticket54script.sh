#!/bin/bash

# Get the current date
date=$(date)

# Get the last 10 users who logged in
last_users=$(last -n 10)

# Get swap space information
swap_space=$(free -h | grep Swap)

# Get kernel version
kernel_version=$(uname -r)

# Get IP address
ip_address=$(ip addr show | grep 'inet ' | awk '{print $2}')

# Output the results to a file
echo "Date: $date" > /tmp/serverinfo.info
echo "Last 10 users who logged in:" >> /tmp/serverinfo.info
echo "$last_users" >> /tmp/serverinfo.info
echo "Swap space: $swap_space" >> /tmp/serverinfo.info
echo "Kernel version: $kernel_version" >> /tmp/serverinfo.info
echo "IP address: $ip_address" >> /tmp/serverinfo.info
